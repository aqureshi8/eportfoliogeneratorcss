/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function start(data) {
    var content = data.content;
    var i = 0;
    while(i < content.length) {
       if(content[i].type === "p")
           addParagraph(content[i]);
       i++;
    }
}

function addParagraph(component) {
    var heading = null;
    if(component.title !== "")
        heading = component.title;
    var p = document.createElement("p");
    p.innerHTML = component.text;
    var div = document.getElementById("c");
    if (heading !== null)
        div.appendChild(heading);
    div.appendChild(p);
}